export interface IMainItem {
  id: number;
  title: string;
}