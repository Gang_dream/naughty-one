import React, { FC } from "react";

import { IMainItem } from "../../models";

interface Props extends IMainItem { }

export const MainItem: FC<Props> = ({ id, title }) => (
  <div key={`list-item-${id}-${title}`} className="App__item">
    {`Title is: ${title}!`}
  </div>
)