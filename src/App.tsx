import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import { generateSequence } from './utils';
import { IMainItem } from './models';
import { MainItem } from './components';
import './App.css';


const App = () => {
    const [itemList, setItemList] = useState<IMainItem[]>([]);

    useEffect(() => {
        setItemList(generateSequence([]));
    }, []);

    //using useCallback here would be an overhead
    const handleAddMoreClick = () => setItemList(generateSequence(itemList));

    return (
        <div className="App">
            <div className="App__header">
                <img src={logo} className="App__logo" alt="logo" />
            </div>
            <div>
                <button className="App__button" onClick={handleAddMoreClick}>
                    Add More
                </button>
            </div>
            <div className="App__item-list">
                {itemList.map(item => <MainItem {...item} />)}
            </div>
        </div>
    );
}

export default App;
