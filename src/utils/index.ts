import { v4 as uuid } from 'uuid';
import { IMainItem } from '../models';

export const generateSequence = (itemList: IMainItem[]) => {
  const items: IMainItem[] = [];

  for (let i = 0; i < 20; i++) {
    items.push({
      //i was not unique, I suppose this change would fit better 
      id: itemList.length + i,
      title: uuid()
    })
  }

  return [...items, ...itemList];
}

//do we really need it? I would prefer using just uuid
// const generateTitle = (length = 10) => {
//   let result: string = '';
//   for (let i = 0; i < length; i++) {
//     result += ('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.charAt(Math.floor(Math.random() *
//       'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.length)));
//   }
//   return result;
// };